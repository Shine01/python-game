# python-game
ball game writen in python
You will need to download a 32x32 ball png file. Name it ball.png and place it in the same directory as game.py

Run it as (Debian based linux):
cd #your directory
host@host:~/Downloads$ ls
ball.png  game.py #and whatever else files just make sure these two files are in the same directory
host@host:~/Downloads$ python game.py
#the output if you lose in the game
host@host:~/Downloads$ #and you're done

#if you have an issue with running the code, email me at pentestwizard@gmail.com or Twitter @_r3c0n_
